import groovy.io.FileType

import java.nio.file.Files
import java.nio.file.Paths

//These should either be added to the script here or specified with command line arguments. If you define them both ways, the version from the command line takes precedence.
// Double backslashes are needed between directories on Windows
specifiedModulePathString = ""
specifiedPluginPathString = ""
specifiedPluginFullName = ""
specifiedPluginClassName = ""
specifiedPluginDescription = ""
specifiedPluginDescription = ""
addExtraPossiblyNeededDependenciesToBuildGradleFile = false


def parseJava(path) {
	if(path.endsWith(".java")){
		def javaFile = new File(path);
		javaFileText = javaFile.text;
		if(javaFileText.contains("org.nrg.xnat.workflow.PipelineEmailHandlerAbst")){
			println "In $path you are referencing the old location of PipelineEmailHandlerAbst. To get your class to work properly, you should change org.nrg.xnat.workflow.PipelineEmailHandlerAbst to org.nrg.xnat.event.listeners.PipelineEmailHandlerAbst\n"
		}
		if(javaFileText.contains("org.nrg.xft.event.Event")){
			println "In $path you are referencing the old version of the XFT event class. To get your class to work properly, you should change org.nrg.xft.event.Event to org.nrg.xft.event.entities.WorkflowStatusEvent"
			if(javaFileText.contains("public void handleEvent")){
				println "You will also need to update your handleEvent method in $path. You should change the method parameters from Event and WrkWorkflowdata to just WorkflowStatusEvent (i.e handleEvent(WorkflowStatusEvent e)). You will now want to get the workflow from the event by adding the following code to the beginning of your handleEvent method:"
				println "if (!(e.getWorkflow() instanceof WrkWorkflowdata)) {return;}"
				println "WrkWorkflowdata wrk = (WrkWorkflowdata)e.getWorkflow();\n"
			}
		}
		if(javaFileText.contains("GetArchiveRootPath")){
			println "In $path you are referencing the GetArchiveRootPath method. The name of that method has changed and you should no longer capitalize the G (so you would reference it as getArchiveRootPath).\n"
		}
		if(javaFileText.contains("implements DicomProjectIdentifier") && !javaFileText.contains("UserI")){
			println "The DicomProjectIdentifier interface has changed to now have the user parameter in the apply method be a UserI rather than an XDATUser. You should update $path to use org.nrg.xft.security.UserI since it implements DicomProjectIdentifier.\n"
		}
		if(javaFileText.contains("TurbineUtils.getUser(")){
			println "In $path you appear to be using the deprecated syntax to get the user object. You should instead get the user object with XDAT.getUserDetails(). Also, in earlier versions of XNAT, the user object returned was a XDATUser. It now returns a UserI, so you should modify your code as needed.\n"
		}
		if(javaFileText.contains(".getSecret()")){
			println "In $path you appear to be using the getSecret method on an alias token. As of XNAT 1.7, the secret is now a String rather than a long. If necessary, you should update how you are using it.\n"
		}
		if(javaFileText.contains("extends SecureResource") && javaFileText.contains("user")){
			println "In $path it looks like you might be trying to reference the user object from SecureResource. In XNAT 1.7, the user object is now private and if you want to use it in classes that extend SecureResource, you will need to do getUser() to obtain it.\n"
		}
		if(javaFileText.contains("siteUrl") || javaFileText.contains("SiteUrl") || javaFileText.contains("SiteURL")){
			println "In $path it looks like you are trying to get the site URL. As of XNAT 1.7, this should be accessed via XDAT.getSiteConfigPreferences().getSiteUrl()\n"
		}
		if(javaFileText.contains(".canRead(")){
			println "In $path it looks like you are doing a permissions check. Your old permissions code may no longer work and you may want to use the Permissions class (e.g. Permissions.canRead(userObject, projectObject) to check if a user can read a project).\n"
		}
		if(javaFileText.contains("@Service") || javaFileText.contains("@Repository") || javaFileText.contains("@XapiRestController") || javaFileText.contains("@RestController") || javaFileText.contains("@Controller") || javaFileText.contains("@NrgPreferenceBean")){
			println "You have a component-based annotation on $path. In order for Spring to register your components, you should add a @ComponentScan annotation to the generated plugin class (e.g. @ComponentScan({\"org.nrg.xnat.workshop.subjectmapping.services.impl\",\"org.nrg.xnat.workshop.subjectmapping.repositories\") ).\n"
		}
		if(javaFileText.contains("Entity")){
			println "You have a Hibernate entity annotation on $path. You should add this class' package to an entityPackages attribute in your plugin class (e.g. if your entity classes were located at org.nrg.xnat.workshop.entities, the @XnatPlugin annotation in your plugin class might look something like: @XnatPlugin(value=\"workshopPlugin\",name=\"Workshop Plugin\",entityPackages=\"org.nrg.xnat.workshop.entities\",dataModels={})).\n"
		}
		if(javaFileText.contains("AdminUtils.getAdminEmailId")){
			println "In $path you should update how you get the admin email address. There is no longer an AdminUtils.getAdminEmailId() method and you should use XDAT.getSiteConfigPreferences().getAdminEmail() or XDAT.getNotificationsPreferences().getHelpContactInfo() instead.\n"
		}
		if(javaFileText.contains("AdminUtils.getMailServer")){
			println "In XNAT 1.7, you can no longer get the mail server via AdminUtils; you should replace AdminUtils.getMailServer() with XDAT.getNotificationsPreferences().getSmtpHostname(). The old syntax was found in $path.\n"
		}
		if(javaFileText.contains("XFT")){
			if(javaFileText.contains("XFT.GetSiteID") || javaFileText.contains("XFT.SetSiteID")){
				println "In XNAT 1.7, you can no longer get and set the site ID via XFT; you should replace XFT.GetSiteID() and XFT.SetSiteID(id) with XDAT.getSiteConfigPreferences().getSiteId() and XDAT.getSiteConfigPreferences().setSiteId(id). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetSiteURL") || javaFileText.contains("XFT.SetSiteURL")){
				println "In XNAT 1.7, you can no longer get and set the site URL via XFT; you should replace XFT.GetSiteURL() and XFT.SetSiteURL(url) with XDAT.getSiteConfigPreferences().getSiteUrl() and XDAT.getSiteConfigPreferences().setSiteUrl(url). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetAdminEmail") || javaFileText.contains("XFT.SetAdminEmail")){
				println "In XNAT 1.7, you can no longer get and set the admin email via XFT; you should replace XFT.GetAdminEmail() and XFT.SetAdminEmail(email) with XDAT.getSiteConfigPreferences().getAdminEmail() and XDAT.getSiteConfigPreferences().setAdminEmail(email). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetAdminEmailHost") || javaFileText.contains("XFT.SetAdminEmailHost")){
				println "In XNAT 1.7, you can no longer get and set the admin email host via XFT; you should replace XFT.GetAdminEmailHost() and XFT.SetAdminEmailHost(host) with XDAT.getNotificationsPreferences().getSmtpHostname() and XDAT.getNotificationsPreferences().getSmtpHostname(host). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetArchiveRootPath") || javaFileText.contains("XFT.SetArchiveRootPath")){
				println "In XNAT 1.7, you can no longer get and set the archive root path via XFT; you should replace XFT.GetArchiveRootPath() and XFT.SetArchiveRootPath(path) with XDAT.getSiteConfigPreferences().getArchivePath() and XDAT.getSiteConfigPreferences().setArchivePath(path). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetPrearchivePath") || javaFileText.contains("XFT.SetPrearchivePath")){
				println "In XNAT 1.7, you can no longer get and set the prearchive path via XFT; you should replace XFT.GetPrearchivePath() and XFT.SetPrearchivePath(path) with XDAT.getSiteConfigPreferences().getArchivePath() and XDAT.getSiteConfigPreferences().setArchivePath(path). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetCachePath") || javaFileText.contains("XFT.SetCachePath")){
				println "In XNAT 1.7, you can no longer get and set the cache path via XFT; you should replace XFT.GetCachePath() and XFT.SetCachePath(path) with XDAT.getSiteConfigPreferences().getCachePath() and XDAT.getSiteConfigPreferences().setCachePath(path). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetPipelinePath") || javaFileText.contains("XFT.SetPipelinePath")){
				println "In XNAT 1.7, you can no longer get and set the pipeline path via XFT; you should replace XFT.GetPipelinePath() and XFT.SetPipelinePath(path) with XDAT.getSiteConfigPreferences().getPipelinePath() and XDAT.getSiteConfigPreferences().setPipelinePath(path). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetRequireLogin") || javaFileText.contains("XFT.SetRequireLogin")){
				println "In XNAT 1.7, you can no longer get and set whether login is required via XFT; you should replace XFT.GetRequireLogin() and XFT.SetRequireLogin(isRequired) with XDAT.getSiteConfigPreferences().getRequireLogin() and XDAT.getSiteConfigPreferences().setRequireLogin(isRequired). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetEmailVerification") || javaFileText.contains("XFT.SetEmailVerification")){
				println "In XNAT 1.7, you can no longer get and set whether email verification is required via XFT; you should replace XFT.GetEmailVerification() and XFT.SetEmailVerification(verificationRequired) with XDAT.getSiteConfigPreferences().getEmailVerification() and XDAT.getSiteConfigPreferences().setEmailVerification(verificationRequired). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetUserRegistration") || javaFileText.contains("XFT.SetUserRegistration")){
				println "In XNAT 1.7, you can no longer get and set whether users are auto-approved via XFT; you should replace XFT.GetUserRegistration() and XFT.SetUserRegistration(autoApprove) with XDAT.getSiteConfigPreferences().getUserRegistration() and XDAT.getSiteConfigPreferences().setUserRegistration(autoApprove). The old syntax was found in $path.\n"
			}
			if(javaFileText.contains("XFT.GetEnableCsrfToken") || javaFileText.contains("XFT.SetEnableCsrfToken")){
				println "In XNAT 1.7, you can no longer get and set whether the CSRF token is enabled via XFT; you should replace XFT.GetEnableCsrfToken() and XFT.SetEnableCsrfToken(enable) with XDAT.getSiteConfigPreferences().getEnableCsrfToken() and XDAT.getSiteConfigPreferences().setEnableCsrfToken(enable). The old syntax was found in $path.\n"
			}
		}
		
		
		
		
		if(javaFileText.contains("AdminUtils.getMailServer()")){
				println "You have a Hibernate entity annotation on $path. You should add this class' package to an entityPackages attribute in your plugin class (e.g. if your entity classes were located at org.nrg.xnat.workshop.entities, the @XnatPlugin annotation in your plugin class might look something like: @XnatPlugin(value=\"workshopPlugin\",name=\"Workshop Plugin\",entityPackages=\"org.nrg.xnat.workshop.entities\",dataModels={})).\n"
		}
		if(javaFileText.contains("AdminUtils.getAdminEmailId()")){
				println "You have a Hibernate entity annotation on $path. You should add this class' package to an entityPackages attribute in your plugin class (e.g. if your entity classes were located at org.nrg.xnat.workshop.entities, the @XnatPlugin annotation in your plugin class might look something like: @XnatPlugin(value=\"workshopPlugin\",name=\"Workshop Plugin\",entityPackages=\"org.nrg.xnat.workshop.entities\",dataModels={})).\n"
		}
	}
}

def processArgs(args) {
	def cli = new CliBuilder(
			usage: 'groovy moduleToPlugin.groovy -m modulePath -p pluginPath -n name -c class -d description -b',
			header: '\nParameters:\n')
	// Create the list of options.
	cli.with {
		h longOpt: 'help', 'Show usage information'
		m longOpt: 'modulePath', args: 1, required: false, 'A String representation of the path to your existing XNAT 1.6 module.'
		p longOpt: 'pluginPath', args: 1, required: false, 'A String representation of the path where you want the code for your new XNAT 1.7 plugin to be located.'
		n longOpt: 'name', args: 1, required: false, 'A String representation of the name of your plugin.'
		c longOpt: 'class', args: 1, required: false, 'A String representation of the name of the plugin java class that will be created for your plugin.'
		d longOpt: 'description', args: 1, required: false, 'A String description of your plugin.'
		b longOpt: 'buildDependencies', required: false, 'Include extra build dependencies that your plugin may or may not use.'
	}

	def options = cli.parse(args)
	// Show usage text when -h or --help option is used.
	if (options.h) {
		cli.usage()
		helpRequested = true
		return
	}

	modulePathString = (options.m) ? options.m : specifiedModulePathString
	pluginPathString = (options.p) ? options.p : specifiedPluginPathString
	pluginFullName = (options.n) ? options.n : specifiedPluginFullName
	pluginClassName = (options.c) ? options.c : specifiedPluginClassName
	pluginDescription = (options.d) ? options.d : specifiedPluginDescription
	buildDependencies = (options.b) ? options.b : addExtraPossiblyNeededDependenciesToBuildGradleFile
}

class Globals {
	static movedFileList = []
	static helpRequested = false
}

println ""
processArgs(args)
 if(!Globals.helpRequested) {
//First we move each directory of files from the module to the corresponding location it should be in the plugin
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "java"], ["src", "main", "java"], this.&parseJava)
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "schemas"], ["src", "main", "resources", "schemas"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "templates"], ["src", "main", "resources", "META-INF", "resources", "templates"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "xnat-templates"], ["src", "main", "resources", "META-INF", "resources", "xnat-templates"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "xdat-templates"], ["src", "main", "resources", "META-INF", "resources", "xdat-templates"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "scripts"], ["src", "main", "resources", "META-INF", "resources", "scripts"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "style"], ["src", "main", "resources", "META-INF", "resources", "style"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "images"], ["src", "main", "resources", "META-INF", "resources", "images"])

	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "main", "java"], ["src", "main", "java"], this.&parseJava)
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "main", "schemas"], ["src", "main", "resources", "schemas"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "main", "templates"], ["src", "main", "resources", "META-INF", "resources", "templates"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "main", "xnat-templates"], ["src", "main", "resources", "META-INF", "resources", "xnat-templates"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "main", "xdat-templates"], ["src", "main", "resources", "META-INF", "resources", "xdat-templates"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "main", "scripts"], ["src", "main", "resources", "META-INF", "resources", "scripts"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "main", "style"], ["src", "main", "resources", "META-INF", "resources", "style"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "main", "images"], ["src", "main", "resources", "META-INF", "resources", "images"])


	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "main", "resources", "module-resources", "java"], ["src", "main", "java"], this.&parseJava)
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "main", "resources", "module-resources", "schemas"], ["src", "main", "resources", "schemas"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "main", "resources", "module-resources", "templates"], ["src", "main", "resources", "META-INF", "resources", "templates"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "main", "resources", "module-resources", "xnat-templates"], ["src", "main", "resources", "META-INF", "resources", "xnat-templates"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "main", "resources", "module-resources", "xdat-templates"], ["src", "main", "resources", "META-INF", "resources", "xdat-templates"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "main", "resources", "module-resources", "scripts"], ["src", "main", "resources", "META-INF", "resources", "scripts"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "main", "resources", "module-resources", "style"], ["src", "main", "resources", "META-INF", "resources", "style"])
	 moveFilesToPlugin(modulePathString, pluginPathString, ["src", "main", "resources", "module-resources", "images"], ["src", "main", "resources", "META-INF", "resources", "images"])

	 moveFilesToPlugin(modulePathString, pluginPathString, ["repository"], ["src", "main", "repository"])

//Next we copy in the gradle files
	 def currDir = new File(getClass().protectionDomain.codeSource.location.path).parent
	 moveFilesToPlugin(currDir, pluginPathString, ["gradle"], ["gradle"])


	 def pathsOfAllFilesInModule = []
	 def dir = new File(modulePathString)
	 dir.eachFileRecurse(FileType.FILES) { file ->
		 pathsOfAllFilesInModule << file.getCanonicalPath()
	 }
	 def allCopied = true
	 def hgDir = Paths.get(modulePathString, ".hg").toString()
	 def gitDir = Paths.get(modulePathString, ".git").toString()

	 for (String pathOfFileInModule : pathsOfAllFilesInModule) {
		 if (!Globals.movedFileList.contains(pathOfFileInModule)) {
			 if (!(pathOfFileInModule.startsWith(gitDir) || pathOfFileInModule.startsWith(hgDir))) {
				 if (allCopied) {
					 println ""
					 allCopied = false
				 }
				 println "Not moved: $pathOfFileInModule"
			 }
		 }
	 }
	 if (allCopied) {
		 println "\nAll files in the module were successfully copied to new locations in the plugin directory.\n"
	 } else {
		 println "\nThis script was unsure what to do with some of the files in your module."
		 println "If you need those files in your plugin, please copy them to the appropriate location in your plugin directory, modifying them if necessary.\n"
	 }


	 try {
		 if (buildDependencies) {
			 gradlewB = new File(Paths.get(currDir, "build.gradle").toString()).newDataInputStream()
		 } else {
			 gradlewB = new File(Paths.get(currDir, "simplebuild.gradle").toString()).newDataInputStream()
		 }
		 def buildDestPath = Paths.get(pluginPathString, "build.gradle").toString()
		 def gradlewDestB = new File(buildDestPath).newDataOutputStream()
		 gradlewDestB << gradlewB
		 gradlewB.close()
		 gradlewDestB.close()
		 println "Created build.gradle file: $buildDestPath"
	 }
	 catch (Throwable e) {

	 }
	 try {
		 gradlew = new File(Paths.get(currDir, "gradlew").toString()).newDataInputStream()
		 def gradlewDestFile = new File(Paths.get(pluginPathString, "gradlew").toString())
		 def gradlewDest = gradlewDestFile.newDataOutputStream()
		 gradlewDest << gradlew
		 gradlewDestFile.setExecutable(true, false)
		 gradlew.close()
		 gradlewDest.close()
	 }
	 catch (Throwable e) {

	 }
	 try {
		 def gradlewBat = new File(Paths.get(currDir, "gradlew.bat").toString()).newDataInputStream()
		 def gradlewBatDestFile = new File(Paths.get(pluginPathString, "gradlew.bat").toString())
		 def gradlewBatDest = gradlewBatDestFile.newDataOutputStream()
		 gradlewBatDest << gradlewBat
		 gradlewBatDestFile.setExecutable(true, false)
		 gradlewBat.close()
		 gradlewBatDest.close()
	 }
	 catch (Throwable e) {

	 }

//Then we create a settings.gradle file, using the name of the destination directory as the rootProject name
	 def pluginPath = Paths.get(pluginPathString)
	 def pluginDirName = pluginPath.getName(pluginPath.getNameCount() - 1)
	 def settingsPath = Paths.get(pluginPathString, "settings.gradle").toString()
	 new File(settingsPath).withWriter { out ->
		 out.println "rootProject.name = '" + pluginDirName + "'"
	 }
	 println "Created settings.gradle file: $settingsPath"

//We then create a plugin class for the plugin so that XNAT treats it as an XNAT plugin	
def classFileNameWithExtension = pluginClassName + ".java"
def javaFilePath = Paths.get(pluginPathString,"src","main","java","org","nrg","xnat","plugin",classFileNameWithExtension)
	 if(!Files.exists(javaFilePath)) {//Don't try to overwrite the plugin class file if it already exists
		 def pluginClassFile = new File(javaFilePath.toString())
		 if (pluginClassFile.getParentFile() != null) {
			 pluginClassFile.getParentFile().mkdirs()
		 }
		 def ls = System.getProperty("line.separator")
		 pluginClassFile.append("package org.nrg.xnat.plugin;")
		 pluginClassFile.append(ls)
		 pluginClassFile.append(ls + "import org.nrg.framework.annotations.XnatDataModel;")
		 pluginClassFile.append(ls + "import org.nrg.framework.annotations.XnatPlugin;")
		 pluginClassFile.append(ls + "import org.springframework.context.annotation.Bean;")
		 pluginClassFile.append(ls + "import org.springframework.context.annotation.ComponentScan;")
		 pluginClassFile.append(ls)
		 def pluginString = /@XnatPlugin(value = "/ + pluginDirName + /", name = "/ + pluginFullName + /", description = "/ + pluginDescription + /")/
		 pluginClassFile.append(ls + pluginString)
		 pluginClassFile.append(ls + "public class " + pluginClassName + " {")
		 pluginClassFile.append(ls + "}")
		 println "Created plugin class file: $javaFilePath"
	 }
	 else{
		 println "Plugin class file already exists. Please modify it manually if you wish to update it: $javaFilePath"	 
	 }
 }
/*
This method is used for moving all the files from a certain directory in the module to the corresponding directory in the plugin.
It takes the path to the module directory, the path to the plugin directory,
an array of the directories that need to be added to the module path to get to the path of the directory within the module that we want to copy,
and an array of the directories that need to be added to the plugin path to get to the path of the directory within the plugin that we want to copy those files to.
*/ 
static void moveFilesToPlugin(String modulePathString,String pluginPathString, ArrayList<String> sourceDirectoryPathWithinModule, ArrayList<String> destinationDirectoryPathWithinPlugin, Closure fileProcesser=null) {
      try{
		def fullStructureToSourceDir = sourceDirectoryPathWithinModule.clone()
		fullStructureToSourceDir.add(0,modulePathString)
		def fullStructureToDestinationDir = destinationDirectoryPathWithinPlugin.clone()
		fullStructureToDestinationDir.add(0,pluginPathString)

		def sourceDir = new File(Paths.get(*fullStructureToSourceDir).toString());
		def fileList = []
		if(sourceDir.exists()) {
			sourceDir.eachFileRecurse(FileType.FILES) { file ->
				fileList << file
			}
			fileList.each {
				def filePath = it.path
				if (fileProcesser != null) {
					fileProcesser(filePath)
				}
				def relativeFilePath = sourceDir.toURI().relativize(new File(it.path).toURI()).getPath()
				def srcFile = new File(filePath)
				def src = srcFile.newDataInputStream()
				def fullStructureToDestinationFile = fullStructureToDestinationDir.clone()
				fullStructureToDestinationFile.add(relativeFilePath)

				def destFile = new File(Paths.get(*fullStructureToDestinationFile).toString())

				if (destFile.getParentFile() != null) {
					destFile.getParentFile().mkdirs()
				}
				def dest = destFile.newDataOutputStream()
				dest << src
				println "Moved: $filePath"
				Globals.movedFileList.add(srcFile.getCanonicalPath())
				src.close()
				dest.close()
			}
		}
	}
	catch(Throwable e){
		println e
		println e.stackTrace
	}
   }  
